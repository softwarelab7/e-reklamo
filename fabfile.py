#
# To be run by Jenkins
#
import time

from fabric.api import *
from fabric.contrib.project import rsync_project


def set_env(environment=''):
    if environment == 'testing':
        env.key_filename = '~/.ssh/ereklamo-testing.pem'
        env.hosts = ['ubuntu@ereklamo-testing.inthe.ph']
        env.remote_dir = '/home/ubuntu/e-reklamo'
#     elif environment == 'staging':
#         env.key_filename = '~/.ssh/lnp-staging.pem'
#         env.hosts = ['ubuntu@lnp.staging.softwarelab7.com']
#     elif environment == 'production':
#         env.key_filename = '~/.ssh/lnp-production.pem'
#         env.hosts = ['ubuntu@lnp.production.softwarelab7.com']

def update_pg_hba():
    rsync_project("/home/ubuntu", "tools/postgres", delete=True)
    sudo("cp postgres/pg_hba.conf /etc/postgresql/9.1/main/pg_hba.conf")
    sudo("rm -rf postgres")
    sudo("service postgresql restart")

def install_postgis():
    sudo("apt-get install -y postgresql-9.1")
    sudo("apt-add-repository -y ppa:sharpie/for-science")
    sudo("apt-add-repository -y ppa:sharpie/postgis-nightly")
    sudo("apt-get update")
    sudo("apt-get install -y postgresql-9.1-postgis")
    with settings(sudo_user="postgres"):
        sudo("createdb template_postgis")
        sudo("psql -d template_postgis -f /usr/share/postgresql/9.1/contrib/postgis-2.1/postgis.sql")
        sudo("psql -d template_postgis -f /usr/share/postgresql/9.1/contrib/postgis-2.1/spatial_ref_sys.sql")
        
def create_postgis_db():
    with settings(sudo_user="postgres"):
        sudo("createdb -T template_postgis ereklamo")

def create_virtualenv():
    sudo("pip install virtualenv")
    sudo("pip install virtualenvwrapper")
    run("mkdir -p /home/ubuntu/virtualenvs")
    create_virtual_env = ["export WORKON_HOME=/home/ubuntu/virtualenvs", 
                          "source /usr/local/bin/virtualenvwrapper.sh", 
                          "mkvirtualenv --no-site-packages ereklamo"]
    run(" && ".join(create_virtual_env))
    
# Temporary only; Use Puppet or Chef in the future
def dependencies():
    core = ['python-dev', 'build-essential']
    psycopg2 = ['libpq-dev', 'python-psycopg2']
    pil = ['libjpeg8-dev', 'zlib1g-dev', 'libfreetype6-dev', 'liblcms1-dev']
    pil_links = ['libjpeg.so', 'libz.so', 'libfreetype.so', 'liblcms.so']
    git = ['git']
    deploy = ['nginx', 'supervisor', 'memcached', 'python-memcache']
    sudo('apt-get install -y %s' % ' '.join(core + psycopg2 + pil + git + deploy))
#     npm = ['announce.js']
#     for module in npm:
#         sudo('npm install %s'  % module)
#         with cd('~/node_modules/%s' % module):
#             run('npm install')  #install dependencies
    with settings(warn_only=True):
        for link in pil_links:
            if run('test -h /usr/lib/%s' % link).failed:
                sudo('ln -s /usr/lib/x86_64-linux-gnu/%s /usr/lib' % link)
#         if run('test -f ~/solr/start.jar').failed:
#             version = '3.6.1'
#             package = 'apache-solr-%s.tgz' % version
#             run('wget -c http://archive.apache.org/dist/lucene/solr/%s/%s' % (version, package))
#             run('tar xzf %s' % package)
#             run('cp -r %s/example ~/solr' % package.replace('.tgz', ''))
#             run('touch ~/solr/solr/conf/stopwords_en.txt')

# Drop tables only instead of the whole database since our DB user doesn't have
# createdb privilege
def drop_all_tables():
    pass

def deploy(local_dir, remote_dir, virtualenv_dir, type):
    run('mkdir -p %s' % remote_dir)
    # local_dir has a slash to copy only the contents
    excluded = ['.git', '.gitignore', '*.pyc']
    rsync_project(remote_dir, local_dir + '/', exclude=excluded, delete=True)

    with cd(remote_dir):
#         run('cp ereklamo/ereklamo/settings/%s.py ereklamo/ereklamo/settings/deploy.py' % (type))
        run('cp ereklamo/ereklamo/settings/%s.py ereklamo/ereklamo/settings/local.py' % (type))
        with settings(warn_only=True):
            if run('test -d %s' % virtualenv_dir).failed:
                run('virtualenv --distribute %s' % virtualenv_dir)
        with prefix('source %s/bin/activate' % virtualenv_dir):
            run('pip install -r requirements/%s.txt' % (type))
            with cd(remote_dir+'/ereklamo'):
                run('python manage.py syncdb --noinput')
                run('python manage.py migrate --noinput')
                run('python manage.py collectstatic --noinput') # to assets
    #             run('python manage.py satchmo_load_l10n')
    #             run('python manage.py initialize_pages')
    #             run('python manage.py initialize_permissions')
    #             run('python manage.py initialize_watermark')
    #             run('python manage.py initialize_shop')
    #             run('python manage.py build_solr_schema > ~/solr/solr/conf/schema.xml')
    #             run('python manage.py rebuild_index --noinput')
        sudo('mkdir -p /var/log/ereklamo') # folder for log files
        sudo('service supervisor stop')
        time.sleep(10)
        sudo('cp tools/supervisor/%s.conf /etc/supervisor/conf.d/ereklamo.conf' % type)
#         sudo('cp tools/memcached/%s.conf /etc/memcached.conf' % type)
        sudo('cp tools/nginx/%s /etc/nginx/sites-available/ereklamo' % type)
        sudo('ln -sf /etc/nginx/sites-available/ereklamo /etc/nginx/sites-enabled/')
        sudo('service supervisor start')
        time.sleep(10)
        sudo('service memcached restart')
        sudo('service nginx restart')

def testing():
    local_dir = '~/bitbucket/e-reklamo'
    remote_dir = '/home/ubuntu/e-reklamo'
    virtualenv_dir = "/home/ubuntu/virtualenvs/ereklamo"
    dependencies()
    deploy(local_dir, remote_dir, virtualenv_dir, 'testing')

# def staging():
#     local_dir = '/var/lib/jenkins/jobs/lnp-staging/workspace/'
#     remote_dir = '/home/ubuntu/lnp'
#     virtualenv_dir = '/home/ubuntu/.virtualenvs/lnp'
#     dependencies()
#     deploy(local_dir, remote_dir, virtualenv_dir, 'staging')
# 
# def production():
#     local_dir = '/var/lib/jenkins/jobs/lnp-production/workspace/'
#     remote_dir = '/home/ubuntu/lnp'
#     virtualenv_dir = '/home/ubuntu/.virtualenvs/lnp'
#     dependencies()
#     deploy(local_dir, remote_dir, virtualenv_dir, 'production')
