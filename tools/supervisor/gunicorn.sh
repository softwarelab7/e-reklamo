#!/bin/bash

. ~/.secrets
~/virtualenvs/ereklamo/bin/python manage.py run_gunicorn -b 'unix:/tmp/gunicorn.sock'
