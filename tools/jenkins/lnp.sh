#!/bin/bash -ex
#
# Will be run by Jenkins lnp project
# Make sure the server has virtualenvwrapper installed.
# Also install dependencies. See fabfile.
#

cd ~/jobs/lnp/workspace
cp tools/settings/ci.py LNP/ci_settings.py
virtualenv --distribute ~/.virtualenvs/lnp
source ~/.virtualenvs/lnp/bin/activate
pip install -r requirements.txt
python manage.py syncdb --noinput
python manage.py migrate
#python manage.py jenkins
