from django.conf.urls import patterns, url
from .views import ReklamoPopupCreateView, ReklamoDetail

urlpatterns = patterns('',
    url(r'popup/$', ReklamoPopupCreateView.as_view(), name='create_reklamo'),
    url(r'(?P<pk>\d+)/$', ReklamoDetail.as_view(), name='detail_reklamo'),
)
