from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import ListView, CreateView, DetailView

from braces.views import (JSONResponseMixin,
                            AjaxResponseMixin, CsrfExemptMixin)

from .models import Reklamo
from .forms import ReklamoModelForm, ReklamoForm
from .gmap_utils import _get_reklamo_location_info
    
    
class ReklamoPopupCreateView(CsrfExemptMixin, JSONResponseMixin, AjaxResponseMixin, CreateView):
    model = Reklamo
    form_class = ReklamoForm
    template_name = 'reklamos/_create_infobubble.html'
    
    def get_form_kwargs(self):
        kwargs = super(ReklamoPopupCreateView, self).get_form_kwargs()
        if self.request.user.is_authenticated():
            user = self.request.user
        else:
            user = None
        kwargs.update({
            "user": user,
            "point": self.request.GET.get('point', None),
            "zoom": self.request.GET.get('zoom', None),
        })
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        json_dict = {
            'form': render_to_string(self.template_name, 
                                        self.get_context_data(form=form),
                                        context_instance=RequestContext(request)),
        }
        return self.render_json_response(json_dict)
                       
    def form_invalid(self, form):
        json_dict = {
            'status': 'ERROR',
            'form': render_to_string(self.template_name, 
                                        self.get_context_data(form=form),
                                        context_instance=RequestContext(self.request)),
        }
        return self.render_json_response(json_dict)
    
    def form_valid(self, form):
        self.object = form.save()
        json_dict = {
            'status': 'OK',
            'new_location': _get_reklamo_location_info(self.object, 'true'),
        }
        return self.render_json_response(json_dict)
    
    
class ReklamoListView(ListView):
    model = Reklamo
    template_name = 'home.html'
    context_object_name = 'reklamos'


class ReklamoDetail(JSONResponseMixin, AjaxResponseMixin, DetailView):
    model = Reklamo
    template_name = 'reklamos/_detail.html'
    context_object_name = 'reklamo'
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        json_dict = {
            'html': render_to_string(self.template_name, 
                                        self.get_context_data(object=self.object), 
                                        context_instance=RequestContext(request))
        }
        return self.render_json_response(json_dict)
