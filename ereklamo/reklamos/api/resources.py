from tastypie import fields
from tastypie.resources import ModelResource

from ..models import Reklamo
from categories.api.resources import CategoryResource 
from users.api.resources import UserResource 


class ReklamoResource(ModelResource):
    reklamador = fields.ForeignKey(UserResource, 'reklamador')
    reklamador_pk = fields.IntegerField(attribute='reklamador_pk', readonly=True)
    reklamador_name = fields.CharField(attribute='reklamador_name', readonly=True)
    
    category = fields.ForeignKey(CategoryResource, 'category')
    category_pk = fields.IntegerField(attribute='category_pk', readonly=True)
    category_name = fields.CharField(attribute='category_name', readonly=True)
    
    longitude = fields.FloatField(attribute='longitude', readonly=True)
    latitude = fields.FloatField(attribute='latitude', readonly=True)

    class Meta:
        queryset = Reklamo.objects.all()
