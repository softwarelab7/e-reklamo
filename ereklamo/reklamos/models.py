from django.contrib.gis.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from core.models import EReklamoBaseModel


class Reklamo(EReklamoBaseModel):
    title = models.CharField(_('Title'), max_length=60)
    body = models.TextField(_('Body'), max_length=2048, blank=True)
    coordinates = models.PointField(_('Map Coordinates'), geography=True, null=True, blank=True)
    coordinate_zoom = models.IntegerField(_('Map Zoom'), default=10)
    category = models.ForeignKey('categories.Category', verbose_name=_('Category'))
    reklamador = models.ForeignKey('auth.User', verbose_name=_('Reklamador'), null=True)
    image = models.ImageField(_('Image'), upload_to='reklamo_imgs', null=True, blank=True)
    
    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Reklamo')
        verbose_name_plural = _('Reklamos')

    @property
    def longitude(self):
        return self.coordinates.x
        
    @property
    def latitude(self):
        return self.coordinates.y

    @property
    def reklamador_pk(self):
        if self.reklamador:
            return self.reklamador.pk
        else:
            return None
        
    @property
    def reklamador_name(self):
        if self.reklamador:
            if self.reklamador.get_full_name():
                return self.reklamador.get_full_name()
            else:
                return self.reklamador.username
        else:
            return _('Di nagpakilala')

    @property
    def category_pk(self):
        return self.category.pk
        
    @property
    def category_name(self):
        return self.category.name
    
    @property
    def image_url(self):
        if self.image:
            return self.image.url
        else:
            return ""
        
    @property
    def absolute_url(self):
        return reverse('home') + "?reklamo=" + self.pk
    
    def get_absolute_url(self):
        return reverse('home') + "?reklamo=" + self.pk

