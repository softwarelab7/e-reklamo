from django import forms

from braces.forms import UserKwargModelFormMixin
                            
from .models import Reklamo
from categories.models import Category


class ReklamoModelForm(forms.ModelForm):
    class Meta:
        model = Reklamo


class ReklamoForm(UserKwargModelFormMixin, forms.ModelForm):
    class Meta:
        model = Reklamo
        exclude = ('reklamador')
        widgets = {
            'coordinates': forms.HiddenInput(),
            'coordinate_zoom': forms.HiddenInput(),
            'title': forms.TextInput(attrs={'class': 'span12', 'placeholder': 'Reklamo title'}),
            'body': forms.Textarea(attrs={'class': 'span12', 'placeholder': 'Explain', 'rows': '2'}),
            'category': forms.Select(attrs={'class': 'span12', 'placeholder': 'Category'}),
            'image': forms.FileInput(attrs={'class': 'span12', 'placeholder': 'Image'}),
        }

    def __init__(self, *args, **kwargs):
        point = kwargs.pop('point', None)
        zoom = kwargs.pop('zoom', None)
        super(ReklamoForm, self).__init__(*args, **kwargs)
        if point:
            self.initial['coordinates'] = point
        if zoom:
            self.initial['coordinate_zoom'] = zoom
        self.fields['category'].queryset = Category.objects.filter(parent__isnull=False)
        
    def save(self, commit=True, *args, **kwargs):
        obj = super(ReklamoForm, self).save(commit=False, *args, **kwargs)
        obj.reklamador = self.user
        if commit:
            obj.save()
        return obj
