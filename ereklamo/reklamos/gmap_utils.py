from django.template.loader import render_to_string

from categories.models import Category


def get_reklamo_location_mapping(reklamos, with_info_bubble):
    locations = []
    if reklamos:
        for r in reklamos:
            if r.coordinates:
                location = _get_reklamo_location_info(r, with_info_bubble)
                locations.append(location)
    return locations
    
def _get_reklamo_location_info(r, with_info_bubble):
    c = r.category
    location = {
        'pk' : r.pk,
        'x_coord' : float(r.coordinates.x),
        'y_coord' : float(r.coordinates.y),
        'zoom' : int(r.coordinate_zoom),
        'title' : r.title,
        'type' : c.parent.pk,
        'cat_pk': c.pk
    }
    if with_info_bubble == 'true':
        info_bubble_content = render_to_string(
                                'reklamos/_detail_infobubble.html',
                                {'reklamo': r,
                                 'category': c})
        location.update({'info_bubble_content': info_bubble_content})
    return location

def get_reklamo_icon_mapping():
    types = {}
    for c in Category.objects.filter(parent__isnull=True):
        types.update({
            c.pk: [c.marker_icon.url, c.cluster_icon.url, c.name]
        })
    return types
    
