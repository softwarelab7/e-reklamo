from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin

from tastypie.api import Api

from categories.api.resources import CategoryResource
from reklamos.api.resources import ReklamoResource
from reklamos.views import ReklamoListView
from users.api.resources import UserResource


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', ReklamoListView.as_view(), name="home"),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^reklamos/', include('reklamos.urls')),
    url(r'^facebook/', include('django_facebook.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(CategoryResource())
v1_api.register(ReklamoResource())

urlpatterns += patterns('',
    url(r'^api/', include(v1_api.urls)),
)
