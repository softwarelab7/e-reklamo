from reklamos.gmap_utils import get_reklamo_location_mapping, get_reklamo_icon_mapping
from reklamos.models import Reklamo

# Add all object-to-location mappings
TYPE_TO_LOCATION_DICT = {
    Reklamo: get_reklamo_location_mapping
}
    
# Add all object-to-icon mappings
TYPE_TO_ICON_DICT = {
    Reklamo: get_reklamo_icon_mapping
}
