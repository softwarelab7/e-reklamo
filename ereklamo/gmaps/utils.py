import json
from datetime import datetime

from .mappings import TYPE_TO_LOCATION_DICT, TYPE_TO_ICON_DICT


json_handler = lambda obj: obj.strftime("%b %d, %Y, %H:%M") if isinstance(obj, datetime) else None

def get_object_model(objects):
    if not objects:
        return None
    return objects[0].__class__
    
def render_gmap(objects, map_width, map_height, map_zoom, 
                with_info_bubble, with_clustering, 
                with_geocode, address_input_box_id, map_name):
    object_model = get_object_model(objects)
    
    locations = types = []
    if object_model:
        locations = TYPE_TO_LOCATION_DICT.get(object_model)(objects, with_info_bubble)
        types =  TYPE_TO_ICON_DICT.get(object_model)()
        
    gmap_widget = '''
    <div id="%(map_name)s" style="width: %(map_width)s; height: %(map_height)s;"></div>
    <script type="text/javascript">
        
        var gmaps_initializers = gmaps_initializers || [];
        
        gmaps_initializers.push(function() {
            var locations = %(locations)s,
                types = %(types)s,
                map_name = "%(map_name)s",
                map_container = $("#"+ map_name).get(0),
                map_zoom = %(map_zoom)d,
                with_info_bubble = %(with_info_bubble)s,
                with_clustering = %(with_clustering)s,
                with_geocode = %(with_geocode)s,
                reklamo_google_map;
            if (typeof google === 'object' && typeof google.maps === 'object') {
                if (with_geocode) {
                    var address_input_box = $("#%(address_input_box_id)s");
                    reklamo_google_map = new ReklamoGoogleMap(map_zoom, map_container, locations, types, with_info_bubble, with_clustering, with_geocode, address_input_box);
                } else {
                    reklamo_google_map = new ReklamoGoogleMap(map_zoom, map_container, locations, types, with_info_bubble, with_clustering, with_geocode);
                }
                reklamo_google_map.initialize();
                ereklamo.maps_in_page.push(reklamo_google_map);
            }
        });
        
    </script>
    ''' % dict(map_name=map_name,
               map_width=map_width,
               map_height=map_height,
               locations=json.dumps(locations, default=json_handler),
               types=json.dumps(types, default=json_handler),
               map_zoom=map_zoom,
               with_info_bubble=with_info_bubble,
               with_clustering=with_clustering,
               with_geocode=with_geocode,
               address_input_box_id=address_input_box_id)
    
    return gmap_widget
