import json

from django import template
from django.conf import settings

from gmaps.utils import render_gmap


register = template.Library()

@register.simple_tag
def gmap_js(infobubble=False, clustering=False):    
    script = """
        <script type="text/javascript" src="%sjs/ereklamo.gmaps.js"></script>
    """ % settings.STATIC_URL
    if infobubble:
        script += """
            <script type="text/javascript" src="%sjs/infobubble.min.js"></script>
        """ % settings.STATIC_URL
    if clustering:
        script += """
            <script type="text/javascript" src="%sjs/markerclusterer.min.js"></script>
        """ % settings.STATIC_URL
        
    script += """
        <script>
            window.gmaps_init_internal = function(){
                $.each(gmaps_initializers, function() {
                    this();
                });
            }
            
            function load_gmap_scripts() {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = "https://maps.googleapis.com/maps/api/js?sensor=false&libraries=geometry&callback=gmaps_init_internal";
                document.body.appendChild(script);
            }
            
            window.onload = load_gmap_scripts;
            
        </script>
    """
    
    return script


@register.simple_tag
def gmap_list(objs, map_width, map_height, map_zoom, map_name):
    return render_gmap(objs, map_width, map_height, map_zoom, 
                        'true', 'true', 'false', None, map_name)
