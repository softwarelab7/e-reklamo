from tastypie.resources import ModelResource
from django.contrib.auth.models import User


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        fields = ['id', 'username', 'email', 'first_name', 'last_name']
