from django.db import models
from django.utils.translation import ugettext_lazy as _

from colorful.fields import RGBColorField

from core.models import EReklamoBaseModel


class Category(EReklamoBaseModel):
    name = models.CharField(_('Name'), max_length=64)
    description = models.TextField(_('Description'), null=True, blank=True)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children', verbose_name='Parent Category')     
    marker_icon = models.FileField(_('Marker Icon'), upload_to='markers', blank=True, null=True)
    cluster_icon = models.FileField(_('Cluster Icon'), upload_to='markers', blank=True, null=True)
    color = RGBColorField(default='#000000')
    
    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    @property
    def parent_pk(self):
        if self.parent:
            return self.parent.pk
        else:
            return None

    @property
    def parent_name(self):
        if self.parent:
            return self.parent.name
        else:
            return None
