# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Category.marker_icon'
        db.add_column(u'categories_category', 'marker_icon',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Category.cluster_icon'
        db.add_column(u'categories_category', 'cluster_icon',
                      self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Category.color'
        db.add_column(u'categories_category', 'color',
                      self.gf('colorful.fields.RGBColorField')(default='#000000', max_length=7),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Category.marker_icon'
        db.delete_column(u'categories_category', 'marker_icon')

        # Deleting field 'Category.cluster_icon'
        db.delete_column(u'categories_category', 'cluster_icon')

        # Deleting field 'Category.color'
        db.delete_column(u'categories_category', 'color')


    models = {
        u'categories.category': {
            'Meta': {'object_name': 'Category'},
            'cluster_icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'color': ('colorful.fields.RGBColorField', [], {'default': "'#000000'", 'max_length': '7'}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marker_icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['categories.Category']"})
        }
    }

    complete_apps = ['categories']