from tastypie import fields
from tastypie.resources import ModelResource

from ..models import Category


class CategoryResource(ModelResource):
    parent = fields.ForeignKey('self', 'parent', null=True)
    parent_pk = fields.IntegerField(attribute='parent_pk', null=True, readonly=True)
    parent_name = fields.CharField(attribute='parent_name', null=True, readonly=True)
    
    class Meta:
        queryset = Category.objects.all()
