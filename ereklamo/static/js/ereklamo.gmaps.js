var ereklamo = ereklamo || {};
ereklamo.maps_in_page = [];

function get_latlng(y, x) {
    return new google.maps.LatLng(y, x)
}

function string_to_latlng(str){
    var latlngStr = str.split(' ');
    var lng = parseFloat(latlngStr[1].substring(1));
    var lat = parseFloat(latlngStr[2].substring(-1));
    return get_latlng(lat, lng)
}

function create_marker(i, instance) {
    var id = i,
        obj_pk = instance.locations[i].pk,
        latlng = get_latlng(instance.locations[i].y_coord, instance.locations[i].x_coord),
        marker_title = instance.locations[i].title,
        marker_type = instance.locations[i].type,
        cat_pk = instance.locations[i].cat_pk,
        marker_icon = instance.types[marker_type][0];
        
    var marker = new google.maps.Marker({
        position: latlng,
        map: instance.map,
        icon: marker_icon,
        title: marker_title,
        zIndex: Math.round(latlng.lat() * -100000) << 5
    });
    
    marker.set('id', id);
    marker.set('obj_pk', obj_pk);
    marker.set('cat_pk', cat_pk);
    
    if (instance.with_info_bubble) {
        var info_window_content = instance.locations[i].info_bubble_content
        marker.set('info_window_content', info_window_content);
                                
        google.maps.event.addListener(marker, 'click', function() {
            instance.info_bubble.setContent(marker.info_window_content);
            instance.info_bubble.open(instance.map, this);
            $('.home-detail').hide();
        });
        
    }
    
    return marker
}


function ReklamoGoogleMap(map_zoom, map_container, 
                          locations, types,
                          with_info_bubble, with_clustering, 
                          with_geocode, address_input_box) {
                          	//console.log(locations);
    this.map;
    this.map_center = get_latlng(14.690566, 121.032362);
    this.map_zoom = map_zoom;
    this.map_container = map_container;
    this.dummy_marker = new google.maps.Marker({
        map: this.map,
        draggable: true,
        icon: ereklamo.STATIC_URL +'images/markers/blue-shadowed.png'
    });
    this.locations = locations;
    this.types = types;
    this.with_info_bubble = with_info_bubble;
    if (this.with_info_bubble) {
        this.info_bubble;
    }
    this.with_clustering = with_clustering;
    if (this.with_clustering) {
        this.markers = {};
        this.marker_clusterer;
        this.list_display = $('.map_object_result');
    }
    this.with_geocode = with_geocode;
    if (this.with_geocode) {
        this.address_input_box = $(address_input_box)
            .addClass('gmap_locate_address')
            .attr('placeholder', 'Enter an address, then drag the marker to tweak the location')
            .width(400);
        this.address_locate_button = $('<input type="button" class="gmap_locate_button btn btn-info" value="Autolocate!">');
        this.geocoder;
    }
}

ReklamoGoogleMap.prototype.initialize = function() {
    this.create_map();
    this.set_center();
    this.add_markers();
    this.cluster_markers();
    this.add_geocode();
}

ReklamoGoogleMap.prototype.create_map = function() {
    var instance = this,
        styles = [
            {
                featureType: "poi",
                stylers: [
                    { visibility: "off" }
                ]
            }
        ],
        styledMap = new google.maps.StyledMapType(styles,
            {name: "Styled Map"}
        ),
        map_options = {
            zoom: instance.map_zoom,
            maxZoom: 21,
            minZoom: 5,
            center: instance.map_center,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                mapTypeId: [google.maps.MapTypeId.ROADMAP, 'map_style']
            },
            navigationControl: true,
        }
    map = new google.maps.Map(instance.map_container, map_options);
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
    instance.map = map;
}

ReklamoGoogleMap.prototype.set_center_by_coordinates = function() {
    var instance = this;
    if (instance.locations.length > 1) {
        this.set_center_by_my_location();
    } else {
        var latlng = get_latlng(instance.locations[0].y_coord, instance.locations[0].x_coord)
        instance.map.setCenter(latlng);
        instance.map.setZoom(instance.locations[0].zoom);
        instance.map_center = latlng;
    }
}

ReklamoGoogleMap.prototype.set_center_by_my_location = function() {
    var instance = this;
    navigator.geolocation.getCurrentPosition(function(position) {
        var latitude = position.coords.latitude,
            longitude = position.coords.longitude;
        var my_location_as_center = get_latlng(latitude, longitude);
        instance.map.setCenter(my_location_as_center);
        instance.map_center = my_location_as_center;
    });
}

ReklamoGoogleMap.prototype.set_center = function() {
    if (this.locations.length > 0) {
        this.set_center_by_coordinates()
    } else {
        this.set_center_by_my_location()
    }
}

ReklamoGoogleMap.prototype.add_markers = function() {
    var instance = this;
        
    if (instance.with_info_bubble) {
        instance.info_bubble = new InfoBubble({
            hideCloseButton: true,
            padding: 10,
            minHeight: 100,
            maxHeight: 320,
            minWidth: 220,
            maxWidth: 220,
            arrowStyle: 0,
            arrowPosition: 50,
            arrowSize: 10,
            shadowStyle: 1,
            borderWidth: 2,
            backgroundColor: '#eeeeee',
            borderColor: '#cccccc',
            borderWidth: 2,
            borderRadius: 10
        });
        
        google.maps.event.addListener(instance.map, "click", function (event) { 
            setTimeout(function () {
                if (!instance.clusterClicked) {
                    var coordinates = event.latLng;
                    instance.dummy_marker.setPosition(coordinates);
                    var point = "POINT (" + coordinates.lng() + " " + coordinates.lat() + ")",
                        zoom = instance.map.getZoom();
                        form = instance.get_create_form(point, zoom);
                    instance.info_bubble.setContent(form);
                    instance.info_bubble.open(instance.map, instance.dummy_marker);
    				ereklamo.sharethis.update_meta();
                    $('.home-detail').hide();
                }
                else {
                    instance.clusterClicked = false;
                }
            }, 0);
        });
        
        instance.bind_create_form();
        
    }
    
    for (var i = 0; i < instance.locations.length; i++) {
        var my_marker = create_marker(i, instance)
        
        if (instance.with_clustering) {
            var type = instance.locations[i].type;
                
            // markers = {type: [{id: marker1, id:marker2, ...}, marker_clusterer]}, {...}
            
            if (type in instance.markers) {
                instance.markers[type][0][my_marker.id] = my_marker
            } else {
                instance.markers[type] = []
                instance.markers[type][0] = {}
                instance.markers[type][0][my_marker.id] = my_marker
            }

        }
    }
    
    
}

ReklamoGoogleMap.prototype.get_create_form = function(point, zoom) {
	var form = "<div class='error'>Error loading! Sorry!</div>";
	$.ajax("/reklamos/popup", {
		async: false,
        data: {'point': point, 'zoom': zoom},
		success: function(data) {
			if (data.form) {
				form = data.form;
			}
		},
	});
	return form;
}

function click_detail () {
    var $obj = $('.detail-reklamo-click');
    $.ajax($obj.attr('data-href'), {
        success: function(data) {
                
            $('.home-detail')
                .html(data.html)
                .show();
                
            var map = ereklamo.maps_in_page[0].map;
            map.panBy(($('.home-detail').width() / 2), 0)
        },
    });
    return false;
}

ReklamoGoogleMap.prototype.bind_create_form = function() {
    var instance = this;
    
    $('#reklamo-map').submit('#reklamo-create', function() {
        var $form = $(this).find('#reklamo-create');
        $.post($form.attr('action'), $form.serialize(),
            function(data){
                if (data.status == 'OK') {
                    instance.info_bubble.close();
                    var i = instance.locations.length;
                    instance.locations.push(data.new_location);
                    var new_marker = create_marker(i, instance);
        
                    if (instance.with_clustering) {
                        var type = instance.locations[i].type;
                        if (type in instance.markers) {
                            instance.markers[type][0][new_marker.id] = new_marker
                        } else {
                            instance.markers[type] = []
                            instance.markers[type][0] = {}
                            instance.markers[type][0][new_marker.id] = new_marker
                        }
                    }
                    
                } else {
                    instance.info_bubble.close();
                    instance.info_bubble.setContent(data.form);
                    instance.info_bubble.open(instance.maps, instance.dummy_marker);
                }
            }, "json");
        return false;
    });
}

ReklamoGoogleMap.prototype.cluster_markers = function() {
    var instance = this;
    if (instance.with_clustering) {
        
        $.each(instance.markers, function(type, cluster_array) {
            
            var marker_map = cluster_array[0],
                markers = [];
            
            $.each(marker_map, function(k, v) {
                markers.push(v);
            });

            cluster_array[1] = new MarkerClusterer(instance.map, markers, {
                maxZoom: 19,
                zoomOnClick: false,
                styles: [{
                    url: instance.types[type][1],
                    height: 64,
                    width: 98,
                    fontFamily: 'Lato',
                    anchor: [16, 0],
                    textColor: '#FFF',
                    textSize: 30
                }]
            });

            
            google.maps.event.addListener(cluster_array[1], 'click', function(cluster) {
                instance.clusterClicked = true;
                var markers_in_cluster = cluster.getMarkers();
                
                var my_cluster = new google.maps.MVCObject;
           		my_cluster.set('position', cluster.getCenter());
                
                var info_window_content_clustered = "<div class='gmap_info_content clustered'><div class='gmap_info_content_wrapper'>"
                                                  + "<h4>"+ instance.types[type][2] +"</h4>"
                                                  + "<ul class='marker_title'>"
           		
                for (var i=0; i<markers_in_cluster.length; i++) {
                    var id = markers_in_cluster[i].id,
                        title = markers_in_cluster[i].getTitle();
                        
                    info_window_content_clustered += "<li data-marker-id='"+ id +"' data-marker-type='"+ type +"'><a>"
                                                   + title
                                                   + "</a></li>"
                }
                
                info_window_content_clustered += "</ul></div></div>";
               
                instance.info_bubble.setContent(info_window_content_clustered);
                $('.home-detail').hide();
                instance.info_bubble.open(instance.map, my_cluster);

                $('.gmap_info_content.clustered li', instance.info_bubble.j).live('click', function() {
                    var current_marker = $(this),
                        current_marker_id = $(current_marker).attr('data-marker-id'),
                        current_marker_type = $(current_marker).attr('data-marker-type');
                    new google.maps.event.trigger(instance.markers[current_marker_type][0][current_marker_id], 'click');
                    instance.map.setZoom(instance.map.maxZoom-5);
                });

            });

        });
        instance.add_map_listeners();
        instance.add_filter_marker_listeners();
    }
}

ReklamoGoogleMap.prototype.add_geocode = function() {
    var instance = this;
    if (instance.with_geocode) {
        instance.geocoder = new google.maps.Geocoder();
        
        $(instance.address_input_box).after($(instance.address_locate_button))
        
        $(instance.address_input_box).keypress(function(e) {
            if (e.keyCode==13) {
                return false;
            }
        });
        
        var marker = new google.maps.Marker({
            map: instance.map,
            draggable: true,
            icon: ereklamo.STATIC_URL +'images/markers/blue-shadowed.png'
        });
        
        if ($('#id_coordinates').length > 0 && $('#id_coordinates').val()) {
            var latLng_str = $('#id_coordinates').val(),
                coordinates = string_to_latlng(latLng_str);
            marker.setPosition(coordinates);
        }
        
        // ASSUMING COORDS INPUT TEXT BOX ID IS #id_coordinates
        google.maps.event.addListener(instance.map, 'click', function(event) {
            var coordinates = event.latLng,
                point = "POINT (" + coordinates.lng() + " " + coordinates.lat() + ")";
            marker.setPosition(coordinates);
            $('#id_coordinates').val(point);
        });

        // ASSUMING COORDS_ZOOM INPUT TEXT BOX ID IS #id_coordinate_zoom
        google.maps.event.addListener(instance.map, 'zoom_changed', function() {
            var zoom_level = instance.map.getZoom();
            $('#id_coordinate_zoom').val(zoom_level);
        });
        
        google.maps.event.addListener(marker, 'dragend', function(event) {
            var coordinates = event.latLng,
                point = "POINT (" + coordinates.lng() + " " + coordinates.lat() + ")";
            $('#id_coordinates').val(point);
        });

        $(instance.address_locate_button).bind('click', function(e) {
            e.preventDefault();
            var address = $(instance.address_input_box).val();
            
            instance.geocoder.geocode({'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $('.map-error').hide();
                    var coordinates = results[0].geometry.location,
                        point = "POINT (" + coordinates.lng() + " " + coordinates.lat() + ")";
                    marker.setPosition(coordinates);
                    instance.map.setCenter(coordinates);
                    instance.map_center = coordinates;
                    $('#id_coordinates').val(point);
                    display_spinner(false);
                } else {
                    marker.setPosition(instance.map_center);
                    if ($('.map-error').length == 0) {
                        $(instance.address_input_box).parent('p')
                            .after('<ul class="errorlist map-error"><li>No results found for the address you entered.</li></ul>');
                    } else {
                        $('.map-error').show();
                    }
                    display_spinner(false);
                }
            });
            return false;
        });
        
        if($(instance.address_input_box).val()) {
            $(instance.address_locate_button).click();
        }
        
    }
}

ReklamoGoogleMap.prototype.add_map_listeners = function() {
    var instance = this;
    
    google.maps.event.addListenerOnce(instance.map, 'idle', function() {
        instance.show_visible_markers();
    });
    
    google.maps.event.addListener(instance.map, 'zoom_changed', function() {
        instance.show_visible_markers();
    });
    
    google.maps.event.addListener(instance.map, 'center_changed', function() {
        instance.show_visible_markers();
    });
        
}

ReklamoGoogleMap.prototype.show_visible_markers = function() {
    var instance = this,
        bounds = instance.map.getBounds(),
        center = instance.map.getCenter();
        
    $.each(instance.markers, function(k, v) {
        v[2] = 0;
        $.each(v[0], function() {
            var marker_position = this.getPosition(),
                marker_distance_to_center = google.maps.geometry.spherical.computeDistanceBetween(center, marker_position);
            this.set('distance_to_center', marker_distance_to_center);
            var list_elem = $(instance.list_display).children('li[data-obj-pk='+ this.obj_pk +']')
            if (bounds.contains(marker_position)) {
                v[2] += 1;
            }
        });
        $('#category-stat-'+ k).find('h2').html(v[2]);
    });
}

ReklamoGoogleMap.prototype.add_filter_marker_listeners = function() {
    var instance = this;
    $.each(this.markers, function(k, v) {
        $('#category-stat-'+ k).on('click', function() {
            if ($(this).hasClass('inactive')) {
                $(this).removeClass('inactive');
                $.each(v[0], function() {
                    this.setVisible(true);
                })
            } else {
                $(this).addClass('inactive');
                $.each(v[0], function() {
                    this.setVisible(false)
                })
            }
        });
        
        $.each(v[0], function() {
            var marker = this,
                clusterer = v[1];
                
            google.maps.event.addListener(marker, 'visible_changed', function() {
                if (marker.getVisible()) {
                    clusterer.addMarker(marker)
                } else {
                    clusterer.removeMarker(marker)
                }                   
            });
            
        });
    });
}
