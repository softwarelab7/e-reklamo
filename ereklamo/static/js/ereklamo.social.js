var ereklamo = ereklamo || {};

function ReklamoShareThis() {
}

ReklamoShareThis.prototype.update_meta = function(title, description, url, image) {
	if (!url || url == "/") {
		url = "";
	}
	url = ereklamo.HOME + url
	title = title || "e-Reklamo!";
	description = description || "Reklamuhan ng bayan!";
	image = image || ereklamo.LOGO;
	
	console.log(url);
	
	$('meta[property="og:title"]').attr("content", title);
	$('meta[property="og:url"]').attr("content", url);
	$('meta[property="og:image"]').attr("content", image);
	$('meta[property="og:description"]').attr("content", description);
	
	$("div.sharethis").each(function() {
		$(this).data("st-url", url);
		$(this).data("st-title", title);
		$(this).data("st-description", description);
		$(this).data("st-image", image);
	});
	
	this.update_buttons();
}

ReklamoShareThis.prototype.update_buttons = function() {

    $("div.sharethis").each(function(i, st) {
        var $st = $(st);
		title = $st.data("st-title") || "e-Reklamo!";
		description = $st.data("st-description") || "Reklamuhan ng bayan!";
		url = $st.data("st-url");
		image = $st.data("st-image") || ereklamo.LOGO;
        $st.removeClass("loading").empty();
	    
		$(".fb-comments").each(function() {
			console.log(url);
			console.log(this);
			$(this).attr("data-href", url);
		});
    	
		try{
	    	FB.XFBML.parse();   //refresh facebook!
		} catch (e) { /* pass */ }
		
        $.each(ereklamo.SOCIAL_SERVICES, function(j, service) {
            var $el = $("<span></span>");
            $st.append($el);
            stWidget.addEntry({
                 "service": service,
                 "type": "hcount",         
                 "element": $el.get(0),
                 "url": url,
                 "title": title,
                 "summary": description,
                 "image": image,
            });
        });
    });
}