from django.forms.widgets import HiddenInput
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import CreateView, View
from django.views.generic.edit import FormMixin, ModelFormMixin

from braces.views import (JSONResponseMixin, AjaxResponseMixin)

from django.views.generic.base import TemplateResponseMixin


class PopupCreateView(ModelFormMixin, JSONResponseMixin, AjaxResponseMixin, TemplateResponseMixin, View):
    hide_fields = ()
        
    def get_form_context(self):
        form = self.get_form(self.get_form_class())
        for hide_field in self.hide_fields:
            form.fields[hide_field].widget = HiddenInput()
        return self.get_context_data(form=form)
    
    def get(self, request, *args, **kwargs):
        self.object = None
        if not self.template_name:
            raise ImproperlyConfigured("Please define template name")
        return self.render_json_response({'form': render_to_string("core/popup_form.html", self.get_form_context(), 
                                                          context_instance=RequestContext(request))})

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
