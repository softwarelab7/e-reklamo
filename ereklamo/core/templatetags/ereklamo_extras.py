from django import template
register = template.Library()

@register.filter
def url_to_absolute(url, request):
	return request.build_absolute_uri(url)
