from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils.models import TimeStampedModel


class EReklamoBaseModel(TimeStampedModel):
    class Meta:
        abstract = True
