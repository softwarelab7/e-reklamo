E-reklamo
=========

1. Setup database
	fab install_postgis
	fab update_pg_hba
	fab create_postgis_db
    psql
        CREATE USER ereklamo WITH PASSWORD '';
        GRANT ALL PRIVILEGES ON DATABASE ereklamo to ereklamo;

2. mkvirtualenv ereklamo

3. pip install -r requirements/<env>.txt

4. python manage.py syncdb --settings=ereklamo.settings.<settings_file_name>

5. python manage.py migrate --settings=ereklamo.settings.<settings_file_name>

6. python manage.py runserver --settings=ereklamo.settings.<settings_file_name>

